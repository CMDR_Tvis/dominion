#name: botania.zs
#maintainer: McMarkS_QM

recipes.remove(<botania:altar>);

recipes.addShaped(
  <botania:altar>,

  [
    [
      <ferdinandsflowers:block_cff_flowersb:10>,
      <ferdinandsflowers:block_cff_flowersb:5>,
      <ferdinandsflowers:block_cff_flowersc:6>
    ],

    [<roots:spirit_herb_item>, <botania:petal:10>, <roots:pereskia_blossom>],
    [<roots:runestone>, <roots:runestone>, <roots:runestone>]
  ]
);
