#name: growthcraft_fishtrap.zs
#maintainer: CMDR_Tvis

recipes.remove(<growthcraft_fishtrap:fishtrap>);

recipes.addShaped(
  <growthcraft_fishtrap:fishtrap>,

  [
    [<ore:stickTreatedWood>, rope, <ore:stickTreatedWood>],

    [
      rope,
      <botania:specialflower>.withTag({type: "hydroangeas"}),
      rope
    ],

    [<ore:stickTreatedWood>, rope, <ore:stickTreatedWood>]
  ]
);
