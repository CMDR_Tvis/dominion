#name: theoneprobe.zs
#maintainer: CMDR_Tvis

import crafttweaker.item.IItemStack;

val disable as IItemStack[] = [
	<theoneprobe:diamond_helmet_probe>,
	<theoneprobe:gold_helmet_probe>,
	<theoneprobe:iron_helmet_probe>,
	<theoneprobe:probe>,
	<theoneprobe:creativeprobe>,
	<theoneprobe:probe_goggles>
];

scripts.utils.registerDisable(disable);
