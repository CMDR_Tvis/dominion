#name: chisel.zs
#maintainer: CMDR_Tvis

import mods.chisel.Carving;
import crafttweaker.item.IItemStack;

Carving.addVariation(
  "block_coal_coke",
  <immersiveengineering:stone_decoration:3>
);

Carving.addVariation("basalt", <undergroundbiomes:igneous_stone:5>);
Carving.addVariation("limestone", <undergroundbiomes:sedimentary_stone>);
Carving.addVariation("marble", <undergroundbiomes:metamorphic_stone:2>);
recipes.remove(<chisel:block_charcoal2:1>);
