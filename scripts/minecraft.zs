#name: minecraft.zs
#maintainer: CMDR_Tvis

import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;

val reducedTooling as IItemStack[] = [
  <minecraft:iron_chestplate>,
  <minecraft:iron_leggings>,
  <minecraft:iron_boots>,
  <minecraft:diamond_shovel>,
  <minecraft:iron_shovel>,
  <minecraft:stone_sword>,
  <minecraft:diamond_sword>,
  <minecraft:iron_sword>,
  <minecraft:golden_shovel>,
  <minecraft:golden_sword>,
  <minecraft:wooden_sword>,
  <minecraft:bow>,
  <minecraft:stone_pickaxe>,
  <minecraft:iron_pickaxe>,
  <minecraft:golden_pickaxe>,
  <minecraft:diamond_pickaxe>,
  <minecraft:golden_axe>,
  <minecraft:diamond_axe>,
  <minecraft:iron_axe>,
  <minecraft:iron_helmet>
];

val reducedToolingAndRemove as IItemStack[] = [
  <minecraft:wooden_pickaxe>,
  <minecraft:golden_leggings>,
  <minecraft:golden_chestplate>,
  <minecraft:golden_helmet>,
  <minecraft:iron_hoe>,
  <minecraft:stone_hoe>,
  <minecraft:golden_boots>,
  <minecraft:diamond_hoe>,
  <minecraft:golden_hoe>,
  <minecraft:wooden_axe>,
  <minecraft:wooden_shovel>,
  <minecraft:stone_shovel>,
  <minecraft:wooden_hoe>, 
  <minecraft:stone_axe>
];

val convert as IIngredient[IItemStack] = {
  <minecraft:cobblestone>:
    <undergroundbiomes:metamorphic_cobble:*> |
    <undergroundbiomes:igneous_cobble:*>,

  <minecraft:mossy_cobblestone>:
    <undergroundbiomes:metamorphic_cobble_mossy:*> |
    <undergroundbiomes:igneous_cobble_mossy:*>,

  <minecraft:stone>:
    <undergroundbiomes:sedimentary_stone:*> |
    <undergroundbiomes:igneous_stone:*> |
    <undergroundbiomes:metamorphic_stone:*>
};

recipes.remove(<minecraft:sugar> * 8);

for i in convert
  recipes.addShapeless(i, [convert[i]]);

scripts.utils.registerReducedTooling(reducedTooling);
scripts.utils.registerReducedToolingAndRemove(reducedToolingAndRemove);
