#name: astralsorcery.zs
#maintainer: CMDR_Tvis

import mods.astralsorcery.LightTransmutation;
import mods.astralsorcery.Altar;
import mods.astralsorcery.StarlightInfusion;
import crafttweaker.item.IItemStack;

val blockSootyMarble as IItemStack[] = [
	<astralsorcery:blockblackmarble>,
	<astralsorcery:blockblackmarble:1>,
	<astralsorcery:blockblackmarble:2>,
	<astralsorcery:blockblackmarble:3>,
	<astralsorcery:blockblackmarble:4>,
	<astralsorcery:blockblackmarble:5>,
	<astralsorcery:blockblackmarble:6>
];

for i in blockSootyMarble {
	<ore:blockSootyMarble>.add(i);
}

Altar.removeAltarRecipe("astralsorcery:shaped/internal/altar/ritualpedestal");

Altar.addAttunementAltarRecipe(
	"astralsorcery:shaped/internal/altar/ritualpedestal",
	<astralsorcery:blockritualpedestal>,
	750,
	150,

	[
		chiseledMarble,
		<ore:gearAquamarine>,
		chiseledMarble,
		marblePillar,
		<botania:pylon>,
		marblePillar,
		bifrostBlock,
		<ore:blockBlaze>,
		bifrostBlock,
		<ore:blockBlaze>,
		<ore:blockBlaze>,
		bifrostBlock,
		bifrostBlock
	]
);

recipes.addShaped(
	<astralsorcery:blockblackmarble> * 4,

	[
		[<ore:stoneMarble>, <astralsorcery:blockmarble>, <ore:stoneMarble>],
		[<ore:stoneMarble>, <ore:coal>, <ore:stoneMarble>],
		[<ore:stoneMarble>, <ore:stoneMarble>, <ore:stoneMarble>]
	]
);

LightTransmutation.removeTransmutation(
	starmetalOre.items[0],
	false
);

LightTransmutation.addTransmutation(
	<botania:storage:2>,
	starmetalOre.items[0],
	500
);

Altar.removeAltarRecipe("astralsorcery:shaped/internal/altar/glasslens");

Altar.addDiscoveryAltarRecipe(
	"astralsorcery:shaped/internal/altar/glasslens",
	glassLens.items[0],
	200,
	200,

	[
		null,
		alfglassPane,
		null,
		alfglassPane,
		<ore:gemAquamarine>,
		alfglassPane,
		null,
		alfglassPane,
		null
	]
);

Altar.removeAltarRecipe("astralsorcery:shaped/internal/altar/sextant");

Altar.addDiscoveryAltarRecipe(
	"astralsorcery:shaped/internal/altar/sextant",
	<astralsorcery:itemsextant>.withTag({astralsorcery: {}}),
	250,
	250,

	[
		null,
		glassLens,
		null,
		<ore:ingotManasteel>,
		glassLens,
		<ore:ingotManasteel>,
		<ore:livingwoodTwig>,
		<ore:ingotGold>,
		<ore:livingwoodTwig>
	]
);

Altar.removeAltarRecipe("astralsorcery:shaped/internal/altar/illuminationpowder");

Altar.addDiscoveryAltarRecipe(
	"astralsorcery:shaped/internal/altar/illuminationpowder",
	illuminationPowder.items[0] * 16,
	200,
	200,

	[
		<ore:dustGlowstone>,
		<ore:dustAquamarine>,
		<ore:dustGlowstone>,
		<ore:dustAquamarine>,
		<botania:spark>,
		<ore:dustAquamarine>,
		<ore:dustGlowstone>,
		<ore:dustAquamarine>,
		<ore:dustGlowstone>
	]
);

Altar.removeAltarRecipe("astralsorcery:shaped/internal/altar/nocturnalpowder");

Altar.addDiscoveryAltarRecipe(
	"astralsorcery:shaped/internal/altar/nocturnalpowder",
	<astralsorcery:itemusabledust:1> * 4,
	200,
	200,

	[
		<ore:dustLapis>,
		<ore:quartzDark>,
		<ore:dustLapis>,
		<ore:quartzDark>,
		illuminationPowder,
		<ore:quartzDark>,
		<ore:dustLapis>,
		<ore:quartzDark>,
		<ore:dustLapis>
	]
);

Altar.removeAltarRecipe("astralsorcery:shaped/internal/altar/illuminator");

Altar.addDiscoveryAltarRecipe(
	"astralsorcery:shaped/internal/altar/illuminator",
	<astralsorcery:blockworldilluminator>,
	300,
	300,

	[
		illuminationPowder,
		<ore:gearAquamarine>,
		illuminationPowder,
		<ore:gearAquamarine>,
		blockOfSunnyQuartz,
		<ore:gearAquamarine>,
		illuminationPowder,
		<ore:gearAquamarine>,
		illuminationPowder
	]
);

Altar.removeAltarRecipe("astralsorcery:shaped/internal/altar/handtelescope");

Altar.addDiscoveryAltarRecipe(
	"astralsorcery:shaped/internal/altar/handtelescope",
	<astralsorcery:blockworldilluminator>,
	300,
	300,

	[
		illuminationPowder,
		<ore:gearAquamarine>,
		illuminationPowder,
		<ore:gearAquamarine>,
		blockOfSunnyQuartz,
		<ore:gearAquamarine>,
		illuminationPowder,
		<ore:gearAquamarine>,
		illuminationPowder
	]
);

StarlightInfusion.removeInfusion(glassLens.items[0]);

StarlightInfusion.addInfusion(
	alfglassPane.items[0],
	glassLens.items[0],
	false,
	0.4,
	200
);

Altar.removeAltarRecipe("astralsorcery:shaped/internal/altar/capebase");

Altar.addTraitAltarRecipe(
	"astralsorcery:shaped/internal/altar/capebase",
	<astralsorcery:itemcape>,
	4500,
	500,

	[
		<ore:dustAstralStarmetal>,
		<ore:plateAstralStarmetal>,
		<ore:dustAstralStarmetal>,
		resonatingGem,
		<ore:leather>,
		resonatingGem,
		spellbindingCloth,
		<astralsorcery:itemcelestialcrystal>,
		spellbindingCloth,
		<ore:plateAstralStarmetal>,
		<ore:plateAstralStarmetal>,
		null,
		null,
		<ore:plateAstralStarmetal>,
		<ore:plateAstralStarmetal>,
		<ore:plateAstralStarmetal>,
		<ore:plateAstralStarmetal>,
		null,
		null, spellbindingCloth,
		spellbindingCloth, null,
		<ore:dustAstralStarmetal>,
		<ore:dustAstralStarmetal>,
		spellbindingCloth,
		<ore:feather>,
		<ore:dustAstralStarmetal>,
		<ore:ingotAstralStarmetal>,
		<ore:enderpearl>
	]
);

Altar.removeAltarRecipe("astralsorcery:shaped/internal/altar/drawingtable");

Altar.addConstellationAltarRecipe(
	"astralsorcery:shaped/internal/altar/drawingtable",
	<astralsorcery:blockmapdrawingtable>,
	2000,
	300,

	[
		naturaPylon,
		null,
		naturaPylon,
		<ore:ingotAstralStarmetal>,
		<ore:blockLapis>,
		<ore:ingotAstralStarmetal>,
		runedMarble,
		<ore:obsidian>,
		runedMarble,
		<ore:gemLapis>,
		<ore:gemLapis>,
		runedMarble,
		runedMarble,
		null,
		null,
		resonatingGem,
		resonatingGem,
		infusedWoodColumn,
		infusedWoodColumn,
		null,
		null
	]
);

Altar.removeAltarRecipe("astralsorcery:shaped/internal/altar/gateway");
