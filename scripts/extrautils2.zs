#name: extrautils2.zs
#maintainer: CMDR_Tvis

import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;

val removeAndAddFurnaceRecipe as IIngredient[IItemStack] = {
  <extrautils2:decorativeglass:4>: blockOfSunnyQuartz,
  <extrautils2:decorativeglass:3>: <botania:quartztypelavender>,
  <extrautils2:decorativeglass:5>: <botania:quartztypered>,
  <extrautils2:decorativeglass>: <ore:blockQuartz>
};

val removeAndAddShaped as IIngredient[][][IItemStack] = {
  <extrautils2:grocket>: [
    [null, <ore:plateSteel>, null],
    [<ore:ingotSteel>, <ore:gemRedstone>, <ore:ingotSteel>],

    [
      <actuallyadditions:item_crystal>,

      <immersiveengineering:conveyor>
        .withTag({conveyorType: "immersiveengineering:extract"}),

      <actuallyadditions:item_crystal>
    ]
  ],

  <extrautils2:pipe> * 8: [
    [<ore:plateSteel>, <ore:ingotSteel>, <ore:plateSteel>],

    [
      <immersiveengineering:conveyor>
        .withTag({conveyorType: "immersiveengineering:conveyor"}),

      <actuallyadditions:item_crystal_empowered:3>,

      <immersiveengineering:conveyor>
        .withTag({conveyorType: "immersiveengineering:conveyor"})
    ],

    [<ore:plateSteel>, <ore:ingotSteel>, <ore:plateSteel>]
  ],

  <extrautils2:redstoneclock>: [
    [
      restoniaCrystal,
      <ore:plateSteel>,
      restoniaCrystal
    ],

    [
      animatedTorch,
      <actuallyadditions:block_misc:7>,
      animatedTorch
    ],

    [
      restoniaCrystal,
      <ore:plateSteel>,
      restoniaCrystal
    ]
  ],

  <extrautils2:magicapple>: [
    [solidifiedExperience, magicalPlanks, solidifiedExperience],
    [magicalPlanks, <ore:foodApple>, magicalPlanks],
    [solidifiedExperience, magicalPlanks, solidifiedExperience]
  ],

  <extrautils2:itembuilderswand>: [
    [null, <ore:plateBrass>, <actuallyadditions:block_placer>],
    [null, <ore:blockMagicalWood>, <ore:plateBrass>],
    [<ore:blockMagicalWood>, null, null]
  ],

  <extrautils2:itemdestructionwand>: [
    [null, <ore:plateBrass>, <actuallyadditions:block_breaker>],
    [null, <ore:blockMagicalWood>, <ore:plateBrass>],
    [<ore:blockMagicalWood>, null, null]
  ],

  <extrautils2:resonator>: [
    [<ore:plateCoal>, animatedTorch, <ore:plateCoal>],
    [<ore:gearCoal>, <ore:gemRedstone>, <ore:gearCoal>],
    [<ore:plateCoal>, <ore:gearArdite>, <ore:plateCoal>]
  ],

  <extrautils2:teleporter:1>: [
    [
      empoweredVoidCrystal,
      <ore:compressed4xCobblestone>,
      empoweredVoidCrystal
    ],

    [
      <ore:compressed4xCobblestone>,
      runeOfTeleportation,
      <ore:compressed4xCobblestone>
    ],

    [
      empoweredVoidCrystal,
      <ore:compressed4xCobblestone>,
      empoweredVoidCrystal
    ]
  ],

  <extrautils2:teleporter>: [
    [
      empoweredEmeradicCrystal,
      <ore:compressed4xDirt>,
      empoweredEmeradicCrystal
    ],

    [
      <ore:compressed4xDirt>,
      runeOfTeleportation,
      <ore:compressed4xDirt>
    ],

    [
      empoweredEmeradicCrystal,
      <ore:compressed4xDirt>,
      empoweredEmeradicCrystal
    ]
  ],

  <extrautils2:wateringcan>: [
    [<ore:plateTin>, null, null],
    [null, <ore:plateTin>, <minecraft:bucket>],
    [null, <ore:plateTin>, <ore:plateTin>]
  ],

  <extrautils2:quarry>: [
    [
      stoneburnt,
      <extrautils2:snowglobe:1>,
      stoneburnt
    ],

    [endCrystal, enderCasing, endCrystal],
    [stoneburnt, eyeOfEnder, stoneburnt]
  ],

  <extrautils2:quarryproxy>: [
    [emeradicCrystal, emeradicCrystal, emeradicCrystal],

    [
      eyeOfEnder,
      <actuallyadditions:block_misc:6>,
      <actuallyadditions:item_drill>
    ],

    [emeradicCrystal, emeradicCrystal, emeradicCrystal]
  ],

  <extrautils2:angelblock>: [
    [null, <ore:obsidian>, null],
    [stoneburnt, <ore:gearGold>, stoneburnt],
    [null, <ore:feather>, null]
  ]
};

for i in removeAndAddFurnaceRecipe {
  recipes.remove(i);
  furnace.addRecipe(i, removeAndAddFurnaceRecipe[i]);
}

scripts.utils.registerRemoveAndAddShaped(removeAndAddShaped);
