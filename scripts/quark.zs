#name: quark.zs
#maintainer: CMDR_Tvis

import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;

val convert as IIngredient[IItemStack] = {
  <quark:bark>: <minecraft:log>,
  <quark:bark:2>: <minecraft:log:2>,
  <quark:bark:1>: <minecraft:log:1>,
  <quark:bark:4>: <minecraft:log2>,
  <quark:bark:5>: <minecraft:log2:1>,
  <quark:bark:3>: <minecraft:log:3>
};

for i in convert {
  val log = convert[i];
  recipes.remove(i);
  recipes.addShaped(i * 4, [[log, log], [log, log]]);
}
