#name: psi.zs
#maintainer: CMDR_Tvis

import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;

val removeAndAddShaped as IIngredient[][][IItemStack] = {
  <psi:cad_battery:2>: [
    [
      <roost:chicken>.withTag({Chicken: "morechickens:brasschicken"}),
      <ore:gemPsi>,
      <roost:chicken>.withTag({Chicken: "morechickens:brasschicken"})
    ],

    [<ore:gemPsi>, <psi:cad_battery:1>, <ore:gemPsi>],

    [
      <roost:chicken>.withTag({Chicken: "morechickens:brasschicken"}),
      <ore:gemPsi>,
      <roost:chicken>.withTag({Chicken: "morechickens:brasschicken"})
    ]
  ],

  <psi:cad_battery>: [
    [<ore:plateBrass>, <ore:dustPsi>, <ore:plateBrass>],
    [<ore:dustPsi>, <thaumcraft:void_seed>, <ore:dustPsi>],
    [<ore:plateBrass>, <ore:dustPsi>, <ore:plateBrass>]
  ],

  <psi:cad_battery:1>: [
    [<ore:blockBrass>, <ore:ingotPsi>, <ore:blockBrass>],
    [<ore:ingotPsi>, <psi:cad_battery>, <ore:ingotPsi>],
    [<ore:blockBrass>, <ore:ingotPsi>, <ore:blockBrass>]
  ],

  <psi:programmer>: [
    [
      <thaumcraft:plank_greatwood>,
      <thaumcraft:plank_greatwood>,
      <thaumcraft:plank_greatwood>
    ],

    [
      <bloodarsenal:blood_diamond>,
      <psi:material>,
      <bloodarsenal:blood_diamond>
    ],

    [<ore:blockBrass>, null, <ore:blockBrass>]
  ],

  <psi:cad_assembler>: [
    [
      <thaumcraft:crystal_aer>,
      <ore:ingotBloodInfusedIron>,
      <thaumcraft:crystal_aer>
    ],

    [
      <bloodmagic:blood_rune:10>,
      <thaumcraft:amber_block>,
      <bloodmagic:blood_rune:10>
    ],

    [
      <thaumcraft:plank_greatwood>,
      <thaumcraft:plank_greatwood>,
      <thaumcraft:plank_greatwood>
    ]
  ]
};

scripts.utils.registerRemoveAndAddShaped(removeAndAddShaped);
