#name: bloodmagic.zs
#maintainer: CMDR_Tvis

import mods.bloodmagic.BloodAltar;
import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;

val removeAndAddShaped as IIngredient[][][IItemStack] = {
  <bloodmagic:altar>: [
    [redNitor, <astralsorcery:iteminfusedglass>, redNitor],
    [ignisVisCrystal, arcaneStone, ignisVisCrystal],
    [coloredLensDamage, <bloodmagic:monster_soul>, coloredLensDamage]
  ],

  <bloodmagic:sacrificial_dagger>: [
    [null, null, <ore:rodAmber>],
    [<thaumcraft:void_seed>, <ore:plateAmber>, null],
    [<ore:rodAstralStarmetal>, <thaumcraft:void_seed>, null]
  ],

  <bloodmagic:soul_forge>: [
    [
      <astralsorcery:itemrockcrystalsimple>,
      <ore:quicksilver>,
      <astralsorcery:itemrockcrystalsimple>
    ],

    [
      arcaneStone,
      <thaumcraft:salis_mundus>,
      arcaneStone
    ],

    [
      arcaneStone,
      <thaumcraft:crystal_ignis>,
      arcaneStone
    ]
  ],
  <bloodmagic:alchemy_table>: [
    [<ore:plateBrass>, <astralsorcery:itemcoloredlens>, <ore:plateBrass>],

    [
      <thaumcraft:crystal_essence>
        .withTag({Aspects: [{amount: 1, key: "ignis"}]}),

      <thaumictinkerer:energetic_nitor>,

      <thaumcraft:crystal_essence>
        .withTag({Aspects: [{amount: 1, key: "ignis"}]})
    ],

    [arcaneStone, bloodOrb, arcaneStone]
  ],

  <bloodmagic:incense_altar>: [
    [<ore:plateBrass>, <ore:plateCoal>, <ore:plateBrass>],
    [<ore:plateBrass>, <minecraft:fire_charge>, <ore:plateBrass>],
    [<ore:plateBrass>, bloodOrb, <ore:plateBrass>]
  ],

  <bloodmagic:lava_crystal>: [
    [
      <thaumcraft:nitor_orange>,
      <ore:oreCrystalFire>,
      <thaumcraft:nitor_orange>
    ],

    [<ore:oreCrystalFire>, bloodOrb, <ore:oreCrystalFire>],

    [
      <thaumcraft:nitor_orange>,
      <ore:oreCrystalFire>,
      <thaumcraft:nitor_orange>
    ]
  ],

  <bloodmagic:ritual_controller:1>: [
    [<ore:obsidian>, arcaneStone, <ore:obsidian>],
    [arcaneStone, bloodOrb, arcaneStone],
    [<ore:obsidian>, arcaneStone, <ore:obsidian>]
  ],

  <bloodmagic:blood_rune:1>: [
    [
      arcaneStone,
      <thaumcraft:matrix_speed>,
      arcaneStone
    ],

    [
      resonatingGem,
      <bloodmagic:blood_rune>,
      resonatingGem
    ],

    [
      arcaneStone,
      arcaneStone,
      arcaneStone
    ]
  ],

  <bloodmagic:blood_rune:10>: [
    [
      resonatingGem,
      <astralsorcery:itemcoloredlens:2>,
      resonatingGem
    ],

    [<ore:dustRedstone>, <bloodmagic:blood_rune>, <ore:dustRedstone>],
    [illuminationPowder, bloodOrb, illuminationPowder]
  ],

  <bloodmagic:blood_rune:6>: [
    [
      arcaneStone,
      <thaumcraft:jar_normal>,
      arcaneStone
    ],

    [
      arcaneStone,
      <bloodmagic:blood_rune>,
      arcaneStone
    ],

    [arcaneStone, <bloodmagic:slate:2>, arcaneStone]
  ],

  <bloodmagic:blood_rune:3>: [
    [
      arcaneStone,
      <bloodarsenal:gem:1>,
      arcaneStone
    ],

    [
      resonatingGem,
      <bloodmagic:blood_rune>,
      resonatingGem
    ],

    [
      arcaneStone,
      resonatingGem,
      arcaneStone
    ]
  ],

  <bloodmagic:blood_rune:4>: [
    [arcaneStone, <bloodarsenal:gem>, arcaneStone],

    [
      resonatingGem,
      <bloodmagic:blood_rune>,
      resonatingGem
    ],

    [
      arcaneStone,
      resonatingGem,
      arcaneStone
    ]
  ],

  <bloodmagic:blood_rune:5>: [
    [
      arcaneStone,

      <forge:bucketfilled>
        .withTag(
          {FluidName: "astralsorcery.liquidstarlight", Amount: 1000}
        ),

      arcaneStone
    ],

    [
      resonatingGem,
      <bloodmagic:blood_rune>,
      resonatingGem
    ],

    [arcaneStone, <bloodmagic:slate:2>, arcaneStone]
  ],

  <bloodmagic:blood_rune:7>: [
    [<ore:obsidian>, bloodOrb, <ore:obsidian>],

    [liquidDeathBucket, <bloodmagic:blood_rune:6>, liquidDeathBucket],

    [<ore:obsidian>, <bloodmagic:slate:3>, <ore:obsidian>]
  ],

  <bloodmagic:blood_rune:8>:  [
    [arcaneStone, bloodOrb, arcaneStone],
    [bloodOrb, <bloodarsenal:slate>, bloodOrb],
    [arcaneStone, bloodOrb, arcaneStone]
  ],

  <bloodmagic:blood_rune:9>: [
    [
      arcaneStone,

      <forge:bucketfilled>
        .withTag(
          {FluidName: "astralsorcery.liquidstarlight", Amount: 1000}
        ),

      arcaneStone
    ],

    [
      <ore:ingotAstralStarmetal>,
      <bloodmagic:blood_rune:1>,
      <ore:ingotAstralStarmetal>
    ],

    [
      arcaneStone,
      <bloodmagic:slate:3>,
      arcaneStone
    ]
  ]
};

recipes.remove(<bloodmagic:blood_rune>);

recipes.addShapeless(
  <bloodmagic:blood_rune>,

  [
    arcaneStone,
    <bloodmagic:slate>
  ]
);

scripts.utils.registerRemoveAndAddShaped(removeAndAddShaped);

recipes.remove(<tconstruct:throwball:1>);

BloodAltar.removeRecipe(<bloodmagic:slate:0>);

BloodAltar.addRecipe(
	<bloodmagic:slate:0>,
	<thaumcraft:plate:2>,
	1,
	1000,
	5,
	5
);
