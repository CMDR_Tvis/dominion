#name: harvestcraft.zs
#maintainer: CMDR_Tvis

import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;

val disable as IItemStack[] = [
  <harvestcraft:market>,
  <harvestcraft:shippingbin>
];

val removeAndAddShaped as IIngredient[][][IItemStack] = {
  <harvestcraft:saucepanitem>: [
    [null, <ore:stickSteel>],
    [<ore:plateSteel>, null]
  ],

  <harvestcraft:skilletitem>: [
    [null, <ore:plateSteel>, <ore:plateSteel>],
    [null, <ore:plateSteel>, <ore:plateSteel>],
    [<ore:stickWood>, null, null]
  ],

  <harvestcraft:cuttingboarditem>: [
    [null, null, <ore:plateSteel>],
    [null, <ore:stickWood>, null],
    [<ore:plankWood>, null, null]
  ],

  <harvestcraft:potitem>: [
    [<ore:plateSteel>, null, <ore:plateSteel>],
    [<ore:plateSteel>, <ore:plateSteel>, <ore:plateSteel>]
  ],

  <harvestcraft:presser>: [
    [
      <ore:plateIron>,
      <immersiveengineering:metal_decoration0:4>,
      <ore:plateIron>
    ],

    [
      <immersiveengineering:material:9>,
      <minecraft:piston>,
      <immersiveengineering:material:9>
    ],

    [<ore:plankWood>, <ore:plankTreatedWood>, <ore:plankTreatedWood>]
  ],

  <harvestcraft:grinder>: [
    [
      <ore:plateIron>,
      <immersiveengineering:metal_decoration0:4>,
      <ore:plateIron>
    ],

    [
      <immersiveengineering:material:9>,
      <ore:gearIron>,
      <immersiveengineering:material:9>
    ],

    [<ore:plankTreatedWood>, <ore:plankTreatedWood>, <ore:plankTreatedWood>]
  ],

  <harvestcraft:market>: [
    [<ore:plankTreatedWood>, eyeOfEnder, <ore:plankTreatedWood>],

    [
      <ore:gearCopper>,
      empoweredEmeradicCrystal,
      <ore:gearCopper>
    ],

    [<ore:plankTreatedWood>, <minecraft:wool:5>, <ore:plankTreatedWood>]
  ],

  <harvestcraft:well>: [
    [<ore:bricksStone>, null, <ore:bricksStone>],

    [
      <ore:bricksStone>,

      <openblocks:tank>
        .onlyWithTag({tank: {FluidName: "water", Amount: 16000}}),

      <ore:bricksStone>
    ],

    [
      <ore:bricksStone>,

      <openblocks:tank>
        .onlyWithTag({tank: {FluidName: "water", Amount: 16000}}),

      <ore:bricksStone>
    ]
  ],

  <harvestcraft:apiary>: [
    [
      <ore:plankTreatedWood>,
      <bibliocraft:framingsheet>,
      <ore:plankTreatedWood>
    ],

    [
      <ore:plankTreatedWood>,
      <bibliocraft:framingsheet>,
      <ore:plankTreatedWood>
    ],

    [<ore:plankTreatedWood>, <bibliocraft:framingsheet>, <ore:plankTreatedWood>]
  ],

  <harvestcraft:waterfilter>: [
    [<ore:plateIron>, <ore:ingotIron>, <ore:plateIron>],
    [<ore:plateIron>, <actuallyadditions:item_filter>, <ore:plateIron>],
    [<ore:plateIron>, <ore:plateIron>, <ore:plateIron>]
  ],

  <harvestcraft:groundtrap>: [
    [<ore:stickTreatedWood>, <openblocks:beartrap>, <ore:stickTreatedWood>],

    [
      <bibliocraft:framingsheet>,
      <actuallyadditions:block_giant_chest>,
      <bibliocraft:framingsheet>
    ],

    [<ore:stickTreatedWood>, <bibliocraft:framingsheet>, <ore:stickTreatedWood>]
  ],

  <harvestcraft:watertrap>: [
    [
      <ore:stickTreatedWood>,
      <actuallyadditions:block_fishing_net>,
      <ore:stickTreatedWood>
    ],

    [
      <bibliocraft:framingsheet>,
      <actuallyadditions:block_giant_chest>,
      <bibliocraft:framingsheet>
    ],

    [<ore:stickTreatedWood>, <bibliocraft:framingsheet>, <ore:stickTreatedWood>]
  ]
};

scripts.utils.registerDisable(disable);
scripts.utils.registerRemoveAndAddShaped(removeAndAddShaped);
