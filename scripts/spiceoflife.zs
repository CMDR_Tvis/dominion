#name: spiceoflife.zs
#maintainer: CMDR_Tvis

import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;

val removeAndAddShaped as IIngredient[][][IItemStack] = {
  <spiceoflife:lunchbox>: [
    [<ore:ingotSteel>, <ore:stickSteel>, <ore:ingotSteel>],
    [<ore:nuggetNickel>, <ore:ingotSteel>, <ore:nuggetNickel>],
    [<ore:plateSteel>, <ore:plateSteel>, <ore:plateSteel>]
  ],
  <spiceoflife:lunchbag>: [
    [<ore:paper>, <actuallyadditions:item_misc>, <ore:paper>],
    [<ore:fiberHemp>, <ore:paper>, <ore:fiberHemp>]
  ],
};

scripts.utils.registerRemoveAndAddShaped(removeAndAddShaped);
