#name: jaopca.zs
#author: CMDR_Tvis
import crafttweaker.item.IItemStack;
import mods.immersiveengineering.MetalPress;

MetalPress.addRecipe(<jaopca:item_platecoal>, <ore:coal>, <immersiveengineering:mold>, 2000);
MetalPress.addRecipe(<jaopca:item_gearcoal>, <ore:coal>, <immersiveengineering:mold:1>, 8000, 4);

MetalPress.addRecipe(<jaopca:item_platediamond>, <ore:gemDiamond>, <immersiveengineering:mold>, 2000);

MetalPress.addRecipe(<jaopca:item_platelapis>, <ore:gemLapis>, <immersiveengineering:mold>, 2000);

MetalPress.addRecipe(<jaopca:item_plateemerald>, <ore:gemEmerald>, <immersiveengineering:mold>, 2000);

MetalPress.addRecipe(<jaopca:item_plateaquamarine>, <ore:gemAquamarine>, <immersiveengineering:mold>, 2000);
MetalPress.addRecipe(<jaopca:item_gearaquamarine>, <ore:gemAquamarine>, <immersiveengineering:mold:1>, 8000, 4);

MetalPress.addRecipe(<jaopca:item_platequartz>, <ore:gemQuartz>, <immersiveengineering:mold>, 2000);
MetalPress.addRecipe(<jaopca:item_gearquartz>, <ore:gemQuartz>, <immersiveengineering:mold:1>, 8000, 4);
