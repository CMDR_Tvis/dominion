#name: openblocks.zs
#maintainer: CMDR_Tvis

import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;

val removeAndAddShaped as IIngredient[][][IItemStack] = {
  <openblocks:guide>: [
    [
      <botania:managlass>,
      restoniaCrystal,
      <botania:managlass>
    ],

    [
      restoniaCrystal,
      <immersiveengineering:metal_decoration2:4>,
      restoniaCrystal
    ],

    [
      <botania:managlass>,
      restoniaCrystal,
      <botania:managlass>
    ]
  ],

  <openblocks:golden_eye:*>: [
    [
      <skyresources:orealchdust:1>,
      <ore:clusterGold>,
      <skyresources:orealchdust:1>
    ],

    [
      <ore:clusterGold>,
      eyeOfEnder,
      <ore:clusterGold>
    ],

    [
      <skyresources:orealchdust:1>,
      <ore:clusterGold>,
      <skyresources:orealchdust:1>
    ]
  ],

  <openblocks:auto_anvil>: [
    [
      <actuallyadditions:item_suction_ring:*>,
      <minecraft:anvil>,
      <actuallyadditions:item_suction_ring:*>
    ],

    [
      restoniaCrystal,
      <actuallyadditions:block_misc:9>,
      restoniaCrystal
    ],

    [
      <actuallyadditions:item_crystal:5>,
      <actuallyadditions:item_crystal:5>,
      <actuallyadditions:item_crystal:5>
    ]
  ],

  <openblocks:village_highlighter>: [
    [
      <thaumcraft:plank_greatwood>,
      <thaumcraft:plank_greatwood>,
      <thaumcraft:plank_greatwood>
    ],

    [
      <thaumcraft:log_greatwood>,
      empoweredEmeradicCrystal,
      <thaumcraft:log_greatwood>
    ],

    [
      arcaneStone,
      arcaneStone,
      arcaneStone
    ],
  ],

  <openblocks:vacuum_hopper>: [
    [
      <ore:obsidian>,
      eyeOfEnder,
      <ore:obsidian>
    ],

    [
      eyeOfEnder,
      <actuallyadditions:item_suction_ring:*>,
      eyeOfEnder
    ],

    [
      <ore:obsidian>,
      eyeOfEnder,
      <ore:obsidian>
    ]
  ],

  <openblocks:rope_ladder>: [
    [rope, <ore:stickWood>, rope],
    [rope, <ore:stickWood>, rope],
    [rope, <ore:stickWood>, rope]
  ],

  <openblocks:fan>: [
    [
      null,
      <ore:stickIron>,
      null
    ],

    [
      <ore:stickIron>,
      <botania:specialflower>.withTag({type: "daffomill"}),
      <ore:stickIron>
    ],

    [
      <ore:plateIron>,
      <extrautils2:ingredients:1>,
      <ore:plateIron>
    ]
  ],

  <openblocks:xp_drain>: [
    [
      <ore:stickSteel>,
      <ore:stickSteel>,
      <ore:stickSteel>
    ],

    [
      <ore:plateSteel>,
      solidifiedExperience,
      <ore:plateSteel>
    ],

    [
      <ore:stickSteel>,
      <ore:stickSteel>,
      <ore:stickSteel>
    ]
  ],

  <openblocks:xp_shower>: [
    [null, <ore:stickSteel>, <immersiveengineering:material:9>],
    [solidifiedExperience, null, null]
  ],

  <openblocks:sprinkler>: [
    [null, <immersiveengineering:material:8>, null],
    [<ore:ingotGold>, <ore:gearGold>, <ore:ingotGold>],
    [null, <immersiveengineering:metal_device1:6>, null]
  ],

  <openblocks:xp_bottler>: [
    [
      <immersiveengineering:metal_device1:6>,
      solidifiedExperience,
      <immersiveengineering:metal_device1:6>
    ],

    [
      <ore:gearIron>,
      <actuallyadditions:block_misc:9>,
      <immersiveengineering:conveyor>
    ],

    [
      <ore:ingotIron>,
      <ore:ingotIron>,
      <ore:ingotIron>
    ]
  ],

  <openblocks:dev_null>: [
    [
      null,
      <ore:dustLapis>,
      null
    ],

    [
      <minecraft:stone_pressure_plate>,
      <extrautils2:minichest>,
      <minecraft:stone_pressure_plate>
    ],

    [
      null,
      restoniaCrystal,
      null
    ]
  ],

  <openblocks:beartrap>: [
    [<minecraft:iron_bars>, null, <minecraft:iron_bars>],
    [<minecraft:iron_bars>, <ore:ingotIron>, <minecraft:iron_bars>],
    [<minecraft:iron_bars>, <ore:gearIron>, <minecraft:iron_bars>]
  ]
};

recipes.remove(<openblocks:builder_guide>);

recipes.addShapeless(
  <openblocks:builder_guide>,
  [<botania:endereyeblock>, <openblocks:guide>]
);

scripts.utils.registerRemoveAndAddShaped(removeAndAddShaped);
