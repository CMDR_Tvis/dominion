#priority 1

#name: utils.zs
#maintainer: CMDR_Tvis

import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;
import mods.actuallyadditions.AtomicReconstructor;

function registerDisable(disable as IItemStack[]) {
  for i in disable {
    recipes.remove(i);
    i.addTooltip(format.darkRed("Disabled"));
  }
}

function registerRemoveAndAddShaped(
  removeAndAddShaped as IIngredient[][][IItemStack]
) {
  for i in removeAndAddShaped {
    recipes.remove(i * 1);
    recipes.addShaped(i, removeAndAddShaped[i]);
  }
}

function registerReducedTooling(tools as IItemStack[]) {
  for i in tools {
    i.maxDamage = maxDamageForReducedTooling;
    i.addTooltip(format.darkRed("Durability reduced"));
  }
}

function registerReducedToolingAndRemove(tools as IItemStack[]) {
  registerReducedTooling(tools);

  for i in tools {
    recipes.remove(i * 1);
  }
}
