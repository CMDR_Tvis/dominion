#priority 1

#name: reference.zs
#maintainer: CMDR_Tvis

import crafttweaker.item.IIngredient;
import crafttweaker.item.IItemStack;

global alfglassPane as IIngredient = <botania:elfglasspane>;
global animatedTorch as IIngredient = <botania:animatedtorch>;
global arcaneStone as IIngredient = <thaumcraft:stone_arcane>;
global bifrostBlock as IIngredient = <botania:bifrostperm>;
global blockOfSunnyQuartz as IIngredient = <botania:quartztypesunny>;

global bloodOrb as IIngredient =
  <bloodmagic:blood_orb>.withTag({orb: "bloodmagic:weak"}) |
  <bloodmagic:blood_orb>.withTag({orb: "bloodmagic:apprentice"}) |
  <bloodmagic:blood_orb>.withTag({orb: "bloodmagic:magician"}) |
  <bloodmagic:blood_orb>.withTag({orb: "bloodmagic:master"}) |
  <bloodmagic:blood_orb>.withTag({orb: "bloodmagic:archmage"});

global chiseledMarble as IIngredient = <astralsorcery:blockmarble:4>;
global clay as IIngredient = <minecraft:clay_ball>;
global coloredLensDamage as IIngredient = <astralsorcery:itemcoloredlens:3>;
global emeradicCrystal as IIngredient = <actuallyadditions:item_crystal:4>;

global empoweredEmeradicCrystal as IIngredient =
  <actuallyadditions:item_crystal_empowered:4>;

global empoweredEnoriCrystal as IIngredient =
  <actuallyadditions:item_crystal_empowered:5>;

global empoweredVoidCrystal as IIngredient =
  <actuallyadditions:item_crystal_empowered:3>;

global endCrystal as IIngredient = <minecraft:end_crystal>;
global enderCasing as IIngredient = <actuallyadditions:block_misc:8>;
global eyeOfEnder as IIngredient = <minecraft:ender_eye>;
global glassLens as IIngredient = <astralsorcery:itemcraftingcomponent:3>;

global ignisVisCrystal as IIngredient = <thaumcraft:crystal_essence>
  .withTag({Aspects: [{amount: 1, key: "ignis"}]});

global illuminationPowder as IIngredient = <astralsorcery:itemusabledust>;
global infusedWoodColumn as IIngredient = <astralsorcery:blockinfusedwood:2>;

global liquidDeathBucket as IIngredient = <forge:bucketfilled>
  .withTag({FluidName: "liquid_death", Amount: 1000});

global magicalPlanks as IIngredient = <extrautils2:decorativesolidwood>;
global marblePillar as IIngredient = <astralsorcery:blockmarble:2>;
global maxDamageForReducedTooling as int = 1;
global naturaPylon as IIngredient = <botania:pylon:1>;
global redNitor as IIngredient = <thaumcraft:nitor_red>;
global resonatingGem as IIngredient = <astralsorcery:itemcraftingcomponent:4>;
global restoniaCrystal as IIngredient = <actuallyadditions:item_crystal>;
global rope as IIngredient = <growthcraft:rope>;
global runedMarble as IIngredient = <astralsorcery:blockmarble:6>;
global runeOfTeleportation as IIngredient = <botanicadds:rune_tp>;

global solidifiedExperience as IIngredient =
  <actuallyadditions:item_solidified_experience>;

global spellbindingCloth as IIngredient = <botania:spellcloth>;
global starmetalOre as IIngredient = <astralsorcery:blockcustomore:1>;
global stoneburnt as IIngredient = <extrautils2:decorativesolid:3>;
