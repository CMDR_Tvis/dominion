#name: immersiveenginering.zs
#maintainer: CMDR_Tvis

import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;

val reducedToolingAndRemove as IItemStack[] = [
	<immersiveengineering:pickaxe_steel>,
	<immersiveengineering:sword_steel>,
	<immersiveengineering:shovel_steel>
];

val removeAndAddShaped as IIngredient[][][IItemStack] = {
	<immersiveengineering:stone_decoration:1> * 3: [
		[<ore:blockSeared>, <minecraft:red_nether_brick>, <ore:blockSeared>],

		[
			<minecraft:red_nether_brick>,
			<ore:blockBlaze>,
			<minecraft:red_nether_brick>
		],

		[<ore:blockSeared>, <minecraft:red_nether_brick>, <ore:blockSeared>]
	],

	<immersiveengineering:stone_decoration:10> * 3: [
		[<minecraft:brick_block>, <ore:blockSeared>, <minecraft:brick_block>],
		[<ore:blockSeared>, <ore:sandstone>, <ore:blockSeared>],
		[<minecraft:brick_block>, <ore:blockSeared>, <minecraft:brick_block>]
	],

	<immersiveengineering:stone_decoration:2> * 3: [
		[<ore:scaffoldingSteel>, <tconstruct:seared_tank>, <ore:scaffoldingSteel>],

		[
			<immersiveengineering:stone_decoration:1>,
			<tconstruct:smeltery_controller>,
			<immersiveengineering:stone_decoration:1>
		],

		[<ore:scaffoldingSteel>, <tconstruct:casting:1>, <ore:scaffoldingSteel>]
	],

	<immersiveengineering:metal_decoration0:6> * 8: [
		[
			<ore:plateSteel>,
			<immersiveengineering:metal_device1:2>,
			<ore:plateSteel>
		],

		[
			<ore:plateElectrum>,
			<botania:specialflower>.withTag({type: "thermalily"}),
			<ore:plateElectrum>
		],

		[
			<ore:plateSteel>,
			<immersiveengineering:metal_device1:2>,
			<immersiveengineering:metal:38>
		]
	],

	<immersiveengineering:metal_decoration0:3> * 2: [
		[<ore:plateIron>, <ore:rodCopper>, <ore:plateIron>],
		[<ore:rodCopper>, animatedTorch, <ore:rodCopper>],
		[<ore:plateIron>, <ore:rodCopper>, <ore:plateIron>]
	],

	<immersiveengineering:metal_decoration0:5> * 4: [
		[<ore:plateSteel>, <immersiveengineering:material:9>, <ore:plateSteel>],
		[<ore:gearDiamond>, <botania:distributor>, <ore:gearDiamond>],
		[<ore:plateSteel>, <immersiveengineering:material:9>, <ore:plateSteel>]
	],

	<immersiveengineering:stone_decoration> * 3: [
		[<tconstruct:dried_clay>, <ore:blockSeared>, <tconstruct:dried_clay>],
		[<ore:blockSeared>, <ore:sandstone>, <ore:blockSeared>],
		[<tconstruct:dried_clay>, <ore:blockSeared>, <tconstruct:dried_clay>]
	],

	<immersiveengineering:metal_decoration0:7> * 8: [
		[<ore:plateSteel>, <ore:plateConstantan>, <ore:plateSteel>],

		[
			<ore:plateConstantan>,
			<botania:specialflower>.withTag({type: "hydroangeas"}),
			<ore:plateConstantan>
		],

		[<ore:plateSteel>, <ore:plateConstantan>, <ore:plateSteel>]
	],

	<immersiveengineering:metal_decoration0:4> * 2: [
		[<ore:plateIron>, <immersiveengineering:material:8>, <ore:plateIron>],
		[<ore:gearIron>, <ore:livingrock>, <ore:gearIron>],
		[<ore:plateIron>, <immersiveengineering:material:8>, <ore:plateIron>]
	],

	<immersiveengineering:metal_device0>: [
		[<ore:plateIron>, <ore:dustSulfur>, <ore:plateIron>],
		[<ore:plateIron>, <ore:plateLead>, <ore:plateIron>],
		[<ore:plankTreatedWood>, <botania:pool:2>, <ore:plankTreatedWood>]
	],

	<immersiveengineering:metal_device0:1>: [
		[<ore:dustSulfur>, <ore:gearLead>, <ore:dustSulfur>],

		[
			<ore:dustRedstone>,
			<immersiveengineering:metal_device0>,
			<ore:dustRedstone>
		],

		[<ore:plateElectrum>, <botania:pool>, <ore:plateElectrum>]
	],

	<immersiveengineering:metal_device0:2>: [
		[<ore:plateSteel>, <ore:plateSteel>, <ore:plateSteel>],

		[
			<ore:gearAluminium>,
			<immersiveengineering:metal_device0:1>,
			<ore:gearAluminium>
		],

		[<ore:plateSteel>, <botania:pool:3>, <ore:plateSteel>]
	],

	<immersiveengineering:metal_decoration0>: [
		[
			<immersiveengineering:wirecoil>,
			<immersiveengineering:wirecoil>,
			<immersiveengineering:wirecoil>
		],

		[
			<immersiveengineering:wirecoil>,
			<ore:stickIron>,
			<immersiveengineering:wirecoil>
		],

		[
			<immersiveengineering:wirecoil>,
			<immersiveengineering:wirecoil>,
			<immersiveengineering:wirecoil>
		]
	],

	<immersiveengineering:metal_decoration0:1>: [
		[
			<immersiveengineering:wirecoil:1>,
			<immersiveengineering:wirecoil:1>,
			<immersiveengineering:wirecoil:1>
		],

		[
			<immersiveengineering:wirecoil:1>,
			<immersiveengineering:material:1>,
			<immersiveengineering:wirecoil:1>
		],

		[
			<immersiveengineering:wirecoil:1>,
			<immersiveengineering:wirecoil:1>,
			<immersiveengineering:wirecoil:1>
		]
	],

	<immersiveengineering:metal_decoration0:2>: [
		[
			<immersiveengineering:wirecoil:2>,
			<immersiveengineering:wirecoil:2>,
			<immersiveengineering:wirecoil:2>
		],

		[
			<immersiveengineering:wirecoil:2>,
			<ore:stickIron>,
			<immersiveengineering:wirecoil:2>
		],

		[
			<immersiveengineering:wirecoil:2>,
			<immersiveengineering:wirecoil:2>,
			<immersiveengineering:wirecoil:2>
		]
	],

	<immersiveengineering:metal_device1:1> * 4: [
		[<ore:plateIron>, <ore:plateConstantan>, <ore:plateIron>],

		[
			<ore:plateConstantan>,
			<immersiveengineering:metal_decoration0>,
			<ore:plateConstantan>
		],

		[
			<ore:plateIron>,
			<botania:specialflower>.withTag({type: "exoflame"}),
			<ore:plateIron>
		]
	],

	<immersiveengineering:metal_device1> * 2: [
		[
			<ore:blockSheetmetalIron>,
			<ore:blockSheetmetalIron>,
			<ore:blockSheetmetalIron>
		],

		[
			<ore:blockSheetmetalIron>,
			<botania:rune:3>,
			<ore:blockSheetmetalIron>
		],

		[
			<immersiveengineering:metal_device0:6>,
			<immersiveengineering:metal_device1:1>,
			<ore:blockSheetmetalIron>
		]
	],

	<immersiveengineering:tool:2>: [
		[<ore:rodCopper>, <minecraft:compass>, <ore:rodCopper>],
		[<ore:stickTreatedWood>, <botania:monocle>, <ore:stickTreatedWood>]
	],

	<immersiveengineering:metal_device1:3> * 8: [
		[<ore:stickSteel>, <ore:plateSteel>, <ore:stickSteel>],

		[
			<botania:specialflower>.withTag({type: "endoflame"}),
			<immersiveengineering:metal_decoration0>,
			<botania:specialflower>.withTag({type: "exoflame"})
		],

		[<ore:plateConstantan>, <ore:plateConstantan>,	<ore:plateConstantan>]
	],

	<immersiveengineering:metal_device1:2>: [
		[
			<ore:plateSteel>,
			<immersiveengineering:material:9>,
			<ore:plateSteel>
		],

		[
			<ore:powderMana>,
			<immersiveengineering:metal_decoration0>,
			<ore:powderMana>
		],

		[
			<ore:plateSteel>,
			<immersiveengineering:material:9>,
			<ore:plateSteel>
		]
	],

	<immersiveengineering:wooden_device1:1>: [
		[
			null,
			<immersiveengineering:material:11>,
			<ore:stickSteel>
		],

		[
			<immersiveengineering:material:11>,
			<immersiveengineering:material:8>,
			<botania:specialflower>.withTag({type: "daffomill"})
		],

		[
			<ore:stickSteel>,
			<immersiveengineering:material:11>,
			<immersiveengineering:material:11>
		]
	],

	<immersiveengineering:wooden_device1>: [
		[
			null,
			<immersiveengineering:material:11>,
			<ore:stickSteel>
		],

		[
			<immersiveengineering:material:11>,
			<immersiveengineering:material:8>,
			<botania:specialflower>.withTag({type: "hydroangeas"})
		],

		[
			<ore:stickSteel>,
			<immersiveengineering:material:11>,
			<immersiveengineering:material:11>
		]
	],

	<immersiveengineering:metal_device1:9>: [
		[
			<ore:plateSteel>,
			<ore:stickSilver>,
			<ore:plateSteel>
		],

		[
			<ore:quartzSunny>,
			<ore:electronTube>,
			<immersiveengineering:metal_decoration0>
		],

		[
			<ore:plateSteel>,
			<ore:stickSilver>,
			<ore:gearIron>
		]
	],

	<immersiveengineering:connector:13>: [
		[null, <immersiveengineering:connector:12>, null],

		[
			<botania:managlass>,
			<immersiveengineering:material:27>,
			<botania:managlass>
		],

		[<botania:managlass>, <ore:quartzRed>, <botania:managlass>]
	],
	<immersiveengineering:metal_device1:8>: [
		[
			<ore:stickAluminum>,
			<immersiveengineering:metal_decoration0:2>,
			<ore:stickAluminum>
		],

		[null, <immersiveengineering:metal_decoration0:2>, null],

		[
			<immersiveengineering:metal_device0:2>,
			<botania:specialflower>.withTag({type: "ba_lightning_flower"}),
			<immersiveengineering:metal_device0:2>
		]
	]
};

scripts.utils.registerRemoveAndAddShaped(removeAndAddShaped);
scripts.utils.registerReducedToolingAndRemove(reducedToolingAndRemove);
