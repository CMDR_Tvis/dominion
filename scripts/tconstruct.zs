#name: tconstruct.zs
#maintainer: CMDR_Tvis

import mods.bloodmagic.BloodAltar;

recipes.remove(<tconstruct:throwball:1>);

BloodAltar.addRecipe(
	<tconstruct:throwball:1>,
	<minecraft:gunpowder>,
	2,
	1500,
	50,
	50
);
