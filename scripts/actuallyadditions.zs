#name: actuallyadditions.zs
#maintainer: CMDR_Tvis

import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;
import mods.actuallyadditions.AtomicReconstructor;

val removeAndAddShaped as IIngredient[][][IItemStack] = {
  <actuallyadditions:block_coal_generator>: [
    [<ore:plateSteel>, <ore:ingotSteel>, <ore:plateSteel>],

    [
      <ore:ingotSteel>,
      <extrautils2:machine>.withTag({Type: "extrautils2:generator"}),
      <ore:ingotSteel>
    ],

    [<ore:plateSteel>, <actuallyadditions:block_misc:9>, <ore:plateSteel>]
  ],

  <actuallyadditions:block_lamp_powerer>: [
    [
      <immersiveengineering:material:27>,
      <immersiveengineering:metal_device1:9>,
      <immersiveengineering:material:27>
    ],

    [
      <actuallyadditions:item_crystal>,
      <immersiveengineering:metal_decoration0:5>,
      <actuallyadditions:item_crystal>
    ],

    [
      <immersiveengineering:material:27>,
      <actuallyadditions:item_crystal>,
      <immersiveengineering:material:27>
    ]
  ],

  <actuallyadditions:block_atomic_reconstructor>: [
    [<ore:plateSteel>, <ore:plateSteel>, <immersiveengineering:material:27>],

    [
      <ore:electronTube>,
      <actuallyadditions:block_misc:9>,
      <immersiveengineering:metal_decoration0:5>
    ],

    [<ore:plateSteel>, <ore:plateSteel>, <immersiveengineering:material:27>]
  ],

  <actuallyadditions:item_misc:6>: [
    [<ore:nuggetGold>, <ore:plateGold>, <ore:nuggetGold>],
    [<ore:plateGold>, null, <ore:plateGold>],
    [<ore:nuggetGold>, <ore:plateGold>, <ore:nuggetGold>]
  ],

  <actuallyadditions:item_misc:8>: [
    [null, <ore:plateGold>, <ore:stickGold>],
    [<ore:plateGold>, <actuallyadditions:item_misc:7>, <ore:plateGold>],
    [<ore:stickGold>, <ore:plateGold>, null]
  ],

  <actuallyadditions:item_misc:7>: [
    [null, <actuallyadditions:item_crystal>, <ore:gemQuartzBlack>],

    [
      <actuallyadditions:item_crystal>,
      <immersiveengineering:wirecoil:2>,
      <actuallyadditions:item_crystal>
    ],

    [<ore:gemQuartzBlack>, <actuallyadditions:item_crystal>, null]
  ],

  <actuallyadditions:block_fermenting_barrel>: [
    [null, <actuallyadditions:item_crystal:5>, null],

    [
      <actuallyadditions:item_crystal:5>,
      <growthcraft_cellar:ferment_barrel>,
      <actuallyadditions:item_crystal:5>
    ],

    [null, <actuallyadditions:item_crystal:5>, null]
  ],

  <actuallyadditions:block_misc:8> * 2: [
    [
      <actuallyadditions:block_misc:6>,
      <actuallyadditions:item_crystal_empowered:2>,
      <actuallyadditions:block_misc:6>
    ],

    [
      <actuallyadditions:block_misc:9>,
      <ore:ingotTerrasteel>,
      <actuallyadditions:block_misc:9>
    ],

    [
      <ore:bEnderAirBottle>,
      <actuallyadditions:item_crystal_empowered:2>,
      <ore:bEnderAirBottle>
    ]
  ]
};

val reconstructorRemoveRecipe as IItemStack[] = [
  <actuallyadditions:item_crystal>,
  <actuallyadditions:item_crystal:1>,
  <actuallyadditions:item_crystal:2>,
  <actuallyadditions:item_crystal:3>,
  <actuallyadditions:item_crystal:4>,
  <actuallyadditions:item_crystal:5>
];

scripts.utils.registerRemoveAndAddShaped(removeAndAddShaped);

for i in reconstructorRemoveRecipe
  AtomicReconstructor.removeRecipe(i);

AtomicReconstructor.addRecipe(
  <actuallyadditions:item_crystal>,
  <ore:powderMana>,
  4000
);

AtomicReconstructor.addRecipe(
  <actuallyadditions:item_crystal:3>,
  <ore:ingotHOPGraphite>,
  6000
);

AtomicReconstructor.addRecipe(
  <actuallyadditions:item_crystal:5>,
  <ore:ingotManasteel>,
  8000
);

AtomicReconstructor.addRecipe(
  <actuallyadditions:item_crystal:2>,
  <ore:manaDiamond>,
  6000
);

AtomicReconstructor.addRecipe(
  <actuallyadditions:item_crystal:4>,
  <ore:dustEmerald>,
  6000
);

AtomicReconstructor.addRecipe(
  <actuallyadditions:item_crystal:1>,
  <botanicadds:mana_lapis>,
  4000
);
