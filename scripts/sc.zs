#name: sc.zs
#maintainer: CMDR_Tvis

import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;

val disable as IItemStack[] = [
  <sc:compresseddirt>,
  <sc:compressedcobblestone>,
  <sc:compressednetherrack>,
  <sc:compressedgravel>,
  <sc:compressedsand>
];

<ore:compressed1xSand>.add(<sc:compressedredsand>);

scripts.utils.registerDisable(disable);
