#name: growthcraft_milk.zs
#maintainer: CMDR_Tvis

import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;

val removeAndAddShaped as IIngredient[][][IItemStack] = {
  <growthcraft_milk:pancheon>: [
    [
      clay,
      <botania:specialflower>.withTag({type: "clayconia"}),
      clay
    ]
  ],

  <growthcraft_milk:churn>: [
    [null, <ore:stickTreatedWood>, null],
    [<ore:stickSteel>, <ore:plankTreatedWood>, <ore:stickSteel>],
    [<ore:plankTreatedWood>, <ore:quartzMana>, <ore:plankTreatedWood>]
  ],

  <growthcraft_milk:cheese_press>: [
    [
      <ore:stickSteel>,
      <immersiveengineering:metal_decoration0:5>,
      <ore:stickSteel>
    ],

    [
      <ore:plankTreatedWood>,
      <bibliocraft:framedchest:*>,
      <ore:plankTreatedWood>
    ],

    [<ore:plankTreatedWood>, <minecraft:piston>, <ore:plankTreatedWood>]
  ]
};

scripts.utils.registerRemoveAndAddShaped(removeAndAddShaped);
