#name: wizardry.zs
#maintainer: McMarkS_QM

import mods.bloodmagic.BloodAltar;
import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;

val removeAndAddShaped as IIngredient[][][IItemStack] = {
  <wizardry:magicians_worktable>: [
    [null, <minecraft:book>, null],

    [
      <botania:dreamwood0slab>,
      <botania:dreamwood0slab>,
      <botania:dreamwood0slab>
    ],

    [<botania:dreamwood>, null, <botania:dreamwood>]
  ],

  <wizardry:crafting_plate>: [
    [null, <botania:rune:8>, null],

    [
      <botania:dreamwood>,
      <wizardry:wisdom_wood_pigmented_planks>,
      <botania:dreamwood>
    ],

    [<botania:dreamwood>, null, <botania:dreamwood>]
  ],

  <wizardry:pearl_holder>: [
    [<minecraft:gold_ingot>, null, <minecraft:gold_ingot>],

    [
      <botania:dreamwood>,
      <wizardry:wisdom_wood_pigmented_planks>,
      <botania:dreamwood>
    ],

    [<botania:dreamwood>, <botania:dreamwood>, <botania:dreamwood>]
  ]
};

scripts.utils.registerRemoveAndAddShaped(removeAndAddShaped);
