#name: animania.zs
#maintainer: CMDR_Tvis

recipes.remove(<animania:block_hamster_wheel>);

recipes.addShaped(
  <animania:block_hamster_wheel>,

  [
    [
      <ore:gearCobalt>,
      empoweredEnoriCrystal,
      <ore:gearCobalt>
    ],

    [
      empoweredEnoriCrystal,
      <ore:gearSilver>,
      empoweredEnoriCrystal
    ],

    [
      <ore:gearCobalt>,
      <ore:compressed5xCobblestone>,
      <ore:gearCobalt>
    ]
  ]
);
