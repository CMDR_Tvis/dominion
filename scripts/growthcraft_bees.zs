#name: growthcraft_bees.zs
#maintainer: CMDR_Tvis

recipes.remove(<growthcraft_bees:beebox>);

recipes.addShaped(
  <growthcraft_bees:beebox>,

  [
    [
      <ore:plankTreatedWood>,
      <ore:clothManaweave>,
      <ore:plankTreatedWood>
    ],

    [
      <ore:plankTreatedWood>,
      <bibliocraft:framedchest:6>,
      <ore:plankTreatedWood>
    ],

    [
      <ore:stickTreatedWood>,
      null,
      <ore:stickTreatedWood>
    ]
  ]
);
