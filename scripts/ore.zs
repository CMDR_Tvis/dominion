#name: ore.zs
#maintainer: CMDR_Tvis

import crafttweaker.item.IItemStack;
import crafttweaker.oredict.IOreDictEntry;

val oreRegistrations as IItemStack[IOreDictEntry] = {
	<ore:ingotMithril>: <embers:ingot_mithril>,
	<ore:nuggetMithril>: <embers:nugget_mithril>,
	<ore:plateMithril>: <embers:plate_mithril>,
	<ore:blockMithril>: <embers:block_mithril>
};

for i in oreRegistrations
	i.add(oreRegistrations[i]);
