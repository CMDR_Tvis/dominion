#name: thaumcraft.zs
#maintainer: McMarkS_QM

import crafttweaker.item.IItemStack;
import mods.thaumcraft.Crucible;
import mods.thaumcraft.ArcaneWorkbench;

val arcaneWorkbenchRemove as IItemStack[] = [
  <thaumcraft:goggles>,
  <thaumcraft:thaumometer>
];

val crucibleRemove as IItemStack[] = [<thaumcraft:ingot:2>];

for i in arcaneWorkbenchRemove
  ArcaneWorkbench.removeRecipe(i);

for i in crucibleRemove
  Crucible.removeRecipe(i);

ArcaneWorkbench.registerShapedRecipe(
  "crafttweaker:goggles",
  "UNLOCKARTIFICE",
  50,
  [],
  <thaumcraft:goggles>,

  [
    [<minecraft:leather>, <thaumcraft:ingot:2>, <minecraft:leather>],
    [<minecraft:leather>, null, <minecraft:leather>],

    [
      <thaumcraft:thaumometer>,
      <embers:aspectus_dawnstone>,
      <thaumcraft:thaumometer>
    ]
  ]
);

ArcaneWorkbench.registerShapedRecipe(
  "crafttweaker:thaumometer",
  "FIRSTSTEPS",
  20,

  [
    <aspect:aer> * 1,
    <aspect:terra> * 1,
    <aspect:aqua> * 1,
    <aspect:ignis> * 1,
    <aspect:ordo> * 1,
    <aspect:perditio> * 1
  ],

  <thaumcraft:thaumometer>,

  [
    [<wizardry:fairy_wings>, <embers:ingot_dawnstone>, <wizardry:sky_dust>],

    [
      <embers:ingot_dawnstone>,
      <astralsorcery:itemcraftingcomponent:3>,
      <embers:ingot_dawnstone>
    ], 

    [<embers:crystal_ember>, <embers:ingot_dawnstone>, <wizardry:fairy_dust>]
  ]
);

Crucible.registerRecipe(
  "crafttweaker:brassingot",
  "METALLURGY@1",
  <thaumcraft:ingot:2>,
  <ore:ingotDawnstone>,
  [<aspect:instrumentum> * 20]
);
