#name: embers.zs
#maintainers: McMarkS_QM, CMDR_Tvis

import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;

val reducedToolingAndRemove as IItemStack[] = [
  <embers:pickaxe_bronze>,
  <embers:pickaxe_silver>,
  <embers:shovel_copper>,
  <embers:sword_bronze>,
  <embers:hoe_lead>,
  <embers:hoe_silver>,
  <embers:shovel_silver>,
  <embers:axe_bronze>,
  <embers:sword_copper>,
  <embers:axe_silver>,
  <embers:shovel_bronze>,
  <embers:shovel_lead>,
  <embers:pickaxe_copper>,
  <embers:pickaxe_lead>,
  <embers:axe_dawnstone>,
  <embers:pickaxe_dawnstone>,
  <embers:axe_lead>,
  <embers:hoe_bronze>,
  <embers:axe_copper>,
  <embers:sword_silver>,
  <embers:hoe_copper>
];

val removeAndAddShaped as IIngredient[][][IItemStack] = {
  <embers:ember_receiver>: [
    [<immersiveengineering:metal:8>, null, <immersiveengineering:metal:8>],
    [<tconstruct:ingots:5>, <embers:plate_caminite>, <tconstruct:ingots:5>]
  ],

  <embers:ember_emitter>: [
    [null, <tconstruct:ingots:5>, null],
    [null, <tconstruct:ingots:5>, null],

    [
      <immersiveengineering:metal:8>,
      <embers:plate_caminite>,
      <immersiveengineering:metal:8>
    ]
  ],

  <embers:crystal_cell>: [
    [<tconstruct:edible:4>, <embers:ember_cluster>, <tconstruct:edible:4>],

    [
      <tconstruct:throwball:1>,
      <embers:ember_cluster>,
      <tconstruct:throwball:1>
    ],

    [<tconstruct:metal:5>, <embers:block_dawnstone>, <tconstruct:metal:5>]
  ],

  <embers:copper_cell>: [
    [
      <embers:block_caminite_brick>,
      <immersiveengineering:metal:8>,
      <embers:block_caminite_brick>
    ],

    [
      <immersiveengineering:metal:8>,
      <tconstruct:metal:5>,
      <immersiveengineering:metal:8>
    ],

    [
      <embers:block_caminite_brick>,
      <immersiveengineering:metal:8>,
      <embers:block_caminite_brick>
    ]
  ],
  <embers:alchemy_pedestal>: [

    [
      <embers:plate_dawnstone>,
      <tconstruct:throwball:1>,
      <embers:crystal_ember>
    ],

    [<embers:ingot_dawnstone>, <embers:mech_core>, <embers:ingot_dawnstone>],

    [
      <embers:stairs_caminite_brick>,
      <embers:block_dawnstone>,
      <embers:stairs_caminite_brick>
    ]
  ],

  <embers:ember_bore>: [
    [
      <embers:stairs_caminite_brick>,
      <tconstruct:ingots:5>,
      <embers:stairs_caminite_brick>
    ],

    [
      <embers:stairs_caminite_brick>,
      <embers:mech_core>,
      <embers:stairs_caminite_brick>
    ],

    [
      <immersiveengineering:metal:8>,
      <immersiveengineering:metal:8>,
      <immersiveengineering:metal:8>
    ]
  ],

  <embers:mech_core>: [
    [
      <immersiveengineering:metal:8>,
      <tconstruct:materials:11>,
      <immersiveengineering:metal:8>
    ],

    [null, <tconstruct:large_plate>.withTag({Material: "manyullyn"}), null],
    [<immersiveengineering:metal:8>, null, <immersiveengineering:metal:8>]
  ]
};

scripts.utils.registerRemoveAndAddShaped(removeAndAddShaped);

<embers:sword_lead>.maxDamage = maxDamageForReducedTooling;

scripts.utils.registerReducedToolingAndRemove(reducedToolingAndRemove);
