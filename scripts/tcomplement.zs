#name: tcomplement.zs
#maintainer: CMDR_Tvis

import crafttweaker.item.IItemStack;

val reducedToolingAndRemove as IItemStack[] = [
  <tcomplement:manyullyn_helmet>,
  <tcomplement:manyullyn_chestplate>,
  <tcomplement:manyullyn_leggings>,
  <tcomplement:manyullyn_boots>,
  <tcomplement:knightslime_helmet>,
  <tcomplement:knightslime_chestplate>,
  <tcomplement:knightslime_leggings>,
  <tcomplement:knightslime_boots>
];

scripts.utils.registerReducedToolingAndRemove(reducedToolingAndRemove);
