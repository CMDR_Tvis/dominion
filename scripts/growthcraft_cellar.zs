#name: growthcraft_cellar.zs
#maintainer: CMDR_Tvis

import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;

val removeAndAddShaped as IIngredient[][][IItemStack] = {
  <growthcraft_cellar:fruit_press>: [
    [
      <ore:scaffoldingSteel>,
      <immersiveengineering:metal_decoration0:5>,
      <ore:scaffoldingSteel>
    ],

    [<ore:scaffoldingSteel>, <minecraft:piston>, <ore:scaffoldingSteel>],
    [<ore:plankTreatedWood>, <ore:plankTreatedWood>, <ore:plankTreatedWood>]
  ],

  <growthcraft_cellar:ferment_barrel>: [
    [<ore:plankTreatedWood>, <ore:plankTreatedWood>, <ore:plankTreatedWood>],
    [<ore:stickSteel>, <ore:powderMana>, <ore:stickSteel>],
    [<ore:plankTreatedWood>, <ore:plankTreatedWood>, <ore:plankTreatedWood>]
  ],

  <growthcraft_cellar:brew_kettle>: [
    [<ore:plateSteel>, <botania:brewery>, <ore:plateSteel>],
    [<ore:plateSteel>, <minecraft:cauldron>, <ore:plateSteel>],
    [<ore:plateSteel>, <botania:alchemycatalyst>, <ore:plateSteel>]
  ],

  <growthcraft_cellar:culture_jar>: [[<ore:slabTreatedWood>], [<botania:vial>]],

  <growthcraft_cellar:brew_kettle_lid>: [
    [null, <ore:nuggetSteel>, null],
    [<ore:plateSteel>, <ore:plateSteel>, <ore:plateSteel>]
  ],

  <growthcraft_cellar:barrel_tap>: [
    [null, <ore:plankTreatedWood>],
    [<ore:plateSteel>, <ore:plateSteel>],
    [null, <ore:plateSteel>]
  ]
};

scripts.utils.registerRemoveAndAddShaped(removeAndAddShaped);
