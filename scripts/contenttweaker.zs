#loader contenttweaker

#name: contenttweaker.zs
#maintainer: CMDR_Tvis

import mods.contenttweaker.VanillaFactory;

val swords as string[] = [
  "sword_air",
  "sword_earth",
  "sword_water"
];

for i in swords {
  val item = VanillaFactory.createItem(i);
  item.maxStackSize = 1;
  item.rarity = "EPIC";
  item.toolClass = "sword";
  item.register();
}
