#name: roost.zs
#maintainer: CMDR_Tvis

import crafttweaker.item.IItemStack;
import crafttweaker.player.IPlayer;
import crafttweaker.item.IIngredient;
import crafttweaker.recipes.ICraftingInfo;
import crafttweaker.formatting.IFormattedText;

val removeAndAddShaped as IIngredient[][][IItemStack] = {
  <roost:catcher>: [
    [null, <botania:vial:1>, <botania:pylon:2>],
    [bifrostBlock, <thaumcraft:plank_greatwood>, <botania:vial:1>],
    [<ore:gaiaIngot>, bifrostBlock, null]
  ],

  <roost:breeder>: [
    [
      <ore:gaiaIngot>,
      <thaumcraft:mirror_essentia>,
      <ore:gaiaIngot>
    ],

    [
      <ore:runeLustB>,
      <botanianeedsit:elementiummanacapacitor:1>,
      <ore:runeLustB>
    ],

    [
      <thaumcraft:plank_greatwood>,
      <thaumcraft:tallow>,
      <thaumcraft:plank_greatwood>
    ]
  ],

  <roost:collector>: [
    [
      <botanicadds:gaiasteel_ingot>,
      <botania:starfield>,
      <botanicadds:gaiasteel_ingot>
    ],

    [
      <botania:storage:1>,
      <botanicadds:terra_catalyst>,
      <botania:storage:1>
    ],

    [
      <thaumcraft:plank_greatwood>,
      <openblocks:vacuum_hopper>,
      <thaumcraft:plank_greatwood>
    ]
  ]
};

recipes.remove(<roost:roost>);

recipes.addShaped(
  <roost:roost>,

  [
    [<ore:gaiaIngot>, <ore:runeGreedB>, <ore:gaiaIngot>],

    [
      <thaumcraft:mirror_essentia>,
      <thaumcraft:infusion_matrix>,
      <thaumcraft:mirror_essentia>
    ],

    [
      <thaumcraft:plank_greatwood>,
      <thaumcraft:amber_block>,
      <thaumcraft:plank_greatwood>
    ]
  ],

  null,

  function (out as IItemStack, cInfo as ICraftingInfo, player as IPlayer) {
    player.sendMessage("§aHamster congratulates you, " + player.name + ". ");
  }
);

scripts.utils.registerRemoveAndAddShaped(removeAndAddShaped);
