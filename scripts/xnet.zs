#name: xnet.zs
#maintainer: CMDR_Tvis

import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;

val removeAndAddShaped as IIngredient[][][IItemStack] = {
	<xnet:wireless_router>: [
		[
			resonatingGem,
			<immersiveengineering:material:27>,
			resonatingGem
		],
		[
			restoniaCrystal,
			enderCasing,
			restoniaCrystal
		],
		[
			resonatingGem,
			<actuallyadditions:item_crystal_empowered>,
			resonatingGem
		]
	],
	<xnet:router>: [
		[
			<immersiveengineering:toolupgrade:9>,
			<immersiveengineering:material:27>,
			<immersiveengineering:toolupgrade:9>
		],
		[
			restoniaCrystal,
			enderCasing,
			restoniaCrystal
		],
		[
			<actuallyadditions:item_crystal:5>,
			resonatingGem,
			<actuallyadditions:item_crystal:5>
		]
	],
	<xnet:controller>: [
		[
			<immersiveengineering:material:27>,
			<actuallyadditions:item_crystal_empowered>,
			<immersiveengineering:material:27>
		],
		[
			restoniaCrystal,
			enderCasing,
			restoniaCrystal
		],
		[
			<actuallyadditions:item_crystal:5>,
			empoweredEnoriCrystal,
			<actuallyadditions:item_crystal:5>
		]
	],
	<xnet:antenna_dish>: [
		[
			null,
			<actuallyadditions:item_crystal:5>,
			null
		],
		[
			<actuallyadditions:item_crystal:5>,
			resonatingGem,
			<actuallyadditions:item_crystal:5>
		],
		[
			null,
			<immersiveengineering:metal_decoration0:2>,
			null
		]
	],
	<xnet:antenna_base>: [
		[
			null,
			<actuallyadditions:item_crystal:5>,
			null
		],
		[
			null,
			<immersiveengineering:metal_decoration0:2>,
			null
		],
		[
			<actuallyadditions:item_crystal:5>,
			enderCasing,
			<actuallyadditions:item_crystal:5>
		]
	],
	<xnet:antenna>: [
		[
			<actuallyadditions:item_crystal:5>,
			<actuallyadditions:item_crystal:5>,
			<actuallyadditions:item_crystal:5>
		],
		[
			<actuallyadditions:item_crystal:5>,
			<immersiveengineering:metal_decoration0:2>,
			<actuallyadditions:item_crystal:5>
		],
		[
			null,
			<actuallyadditions:item_crystal:5>,
			null
		]
	],
	<xnet:facade>: [
		[<ore:plateSteel>, <ore:paper>, <ore:plateSteel>],
		[<ore:paper>, <actuallyadditions:block_misc:9>, <ore:paper>],
		[<ore:plateSteel>, <minecraft:paper>, <ore:plateSteel>]
	],
	<xnet:redstone_proxy>: [
		[
			<actuallyadditions:item_crystal:5>,
			restoniaCrystal,
			<actuallyadditions:item_crystal:5>
		],
		[
			restoniaCrystal,
			<actuallyadditions:block_misc:9>,
			restoniaCrystal
		],
		[
			<actuallyadditions:item_crystal:5>,
			restoniaCrystal,
			<actuallyadditions:item_crystal:5>
		]
	]
};

scripts.utils.registerRemoveAndAddShaped(removeAndAddShaped);
