#!/usr/bin/env sh

RED="\033[1;31m"

GREEN="\033[0;32m"

YELLOW="\033[1;33m"

RESET="\033[0m"

MEMORY="$1"

FORGE_VERSION="1.12.2-14.23.5.2838"

if [ -z "${MEMORY}" ]
then
  echo "${RED}Specify allocated RAM in GB! Example: ./form_server.sh 2${RESET}"
  exit
fi

echo "${GREEN}Downloading Minecraft server. ${YELLOW}wget package must be installed!${RESET}"
wget https://files.minecraftforge.net/maven/net/minecraftforge/forge/${FORGE_VERSION}/forge-${FORGE_VERSION}-installer.jar
java -Dfile.encoding=UTF-8 -jar -Xmx${MEMORY}G -Xms${MEMORY}G -XX:+UseCompressedOops -XX:+OptimizeStringConcat -XX:+AggressiveOpts -XX:+UseFastAccessorMethods forge-${FORGE_VERSION}-installer.jar --installServer
rm forge-${FORGE_VERSION}-installer.jar
rm forge-${FORGE_VERSION}-installer.jar.log
mv forge-${FORGE_VERSION}-universal.jar core.jar

echo "${GREEN}Removing client-only stuff${RESET}"
rm -r resources/
cd mods
rm AppleSkin-mc1.12-1.0.9.jar
rm BetterAdvancements-1.12.2-0.1.0.77.jar
rm BetterFps-1.4.8.jar
rm CustomBackgrounds-MC1.12-1.1.1
rm foamfix-0.10.5-1.12.2.jar
rm InventoryTweaks-1.63.jar
rm itlt-1.12-1.0.0.jar
rm jehc-1.12.2-1.6.4.jar
rm jeiintegration_1.12.2-1.5.1.36.jar
rm MapWriter-1.12.2-2.8.2.jar
rm moreoverlays-1.14-mc1.12.2.jar
rm MouseTweaks-2.10-mc1.12.2.jar
rm OptiFine_1.12.2_HD_U_E3.jar
rm ResourceLoader-MC1.12.1-1.5.3.jar
rm SmoothFont-mc1.12.2-2.0.jar
rm ThaumicJEI-1.12.2-1.6.0-27.jar
cd ..

echo "${GREEN}Accepting Minecraft EULA for you${RESET}"
echo 'eula=true' > eula.txt

echo "${GREEN}Writing additional scripts${RESET}"

echo "
#!/usr/bin/env bash

while true
do
java -Dfile.encoding=UTF-8 -jar -Xmx${MEMORY}G -Xms${MEMORY}G -XX:+UseNUMA -XX:+CMSParallelRemarkEnabled -XX:+UseBiasedLocking -XX:+UseFastAccessorMethods -XX:+UseCompressedOops -XX:+OptimizeStringConcat -XX:+AggressiveOpts -XX:+UseCodeCacheFlushing ./core.jar
echo '${YELLOW}If you want to completely stop the server process now, press Ctrl+C before the time is up!${RESET}'
echo '${GREEN}Automated reloading in:${RESET}'
for i in 5 4 3 2 1
do
echo \"${GREEN}\$i...${RESET}\"
sleep 1
done
echo '${GREEN}Reloading${RESET}'
done
" > autorestart.sh
chmod +x autorestart.sh

echo "
#!/usr/bin/env bash

echo '${GREEN}Updating server. ${YELLOW}git package must be installed!${RESET}'
git reset --hard
git pull
chmod +x form_server.sh
./form_server.sh ${MEMORY}
" > update_server.sh
chmod +x update_server.sh
